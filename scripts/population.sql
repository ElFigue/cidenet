-- areas
INSERT INTO areas(name,description)
VALUES ('administracion','');

INSERT INTO areas(name,description)
VALUES ('financiera','');

INSERT INTO areas(name,description)
VALUES ('compras','');

INSERT INTO areas(name,description)
VALUES ('infraestructura','');

INSERT INTO areas(name,description)
VALUES ('operacion','');

INSERT INTO areas(name,description)
VALUES ('talento humano','');

INSERT INTO areas(name,description)
VALUES ('servicios varios','');

INSERT INTO areas(name,description)
VALUES ('otra','otra área');

-- type_id
INSERT INTO type_id(name,description)
VALUES ('cedula de ciudadania','Todo trabajador Colombiano debera de tener su Cedula de Ciudadania.');

INSERT INTO type_id(name,description)
VALUES ('cedula de extranjeria','Solo para Extranjeros que no tengan Cedula Colombiana.');

INSERT INTO type_id(name,description)
VALUES ('pasaporte','Valido para Todas las personas como Documento de identificación.');

INSERT INTO type_id(name,description)
VALUES ('permiso especial','Casos muy Especiales.');

-- countries
INSERT INTO countries(name)
VALUES ('Colombia');-- 1

INSERT INTO countries(name)
VALUES ('Estados Unidos');-- 2


