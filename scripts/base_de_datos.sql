CREATE DATABASE cidenet;

USE cidenet;

CREATE TABLE areas(
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(120) NOT NULL,
    description TEXT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE type_id(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(120) NOT NULL,
    description TEXT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE countries(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE employees(
    id INT NOT NULL AUTO_INCREMENT,
    number VARCHAR(20) UNIQUE,
    lastname VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    name VARCHAR(20) NOT NULL,
    middle_name VARCHAR(50) NULL,
    countries_id INT NOT NULL,
    type_id_id INT NOT NULL,
    email VARCHAR(300) NOT NULL UNIQUE,
    areas_id INT NOT NULL,
    status TINYINT NULL DEFAULT 1,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id),
    FOREIGN KEY (countries_id) REFERENCES countries(id),
    FOREIGN KEY (type_id_id) REFERENCES type_id(id),
    FOREIGN KEY (areas_id) REFERENCES areas(id)
);

CREATE TABLE registers(
    id INT NOT NULL AUTO_INCREMENT,
    type_register VARCHAR(20) NOT NULL, -- entrada o salida
    registers_date DATETIME NOT NULL DEFAULT NOW(),
    employees_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (employees_id) REFERENCES employees(id)
);
