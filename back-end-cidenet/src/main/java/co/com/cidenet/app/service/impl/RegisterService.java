package co.com.cidenet.app.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import co.com.cidenet.app.model.Register;
import co.com.cidenet.app.repository.IRegisterRepository;
import co.com.cidenet.app.service.iface.IRegisterService;

@Service
public class RegisterService implements IRegisterService{
	
	

	@Autowired
	private IRegisterRepository registerRepository;
	
	@Override
	public Register save(Register register) {
		return registerRepository.save(register);
	}

	@Override
	public Page<Register> show() {
		// TODO Auto-generated method stub
		return null;
	}

}
