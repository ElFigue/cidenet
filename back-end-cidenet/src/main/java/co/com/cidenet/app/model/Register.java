package co.com.cidenet.app.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "registers")
public class Register implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "type_register")
	private String typeRegister;
	
	@Column(name = "registers_date")
	private LocalDateTime registersDate;
	
	@ManyToOne
	@JoinColumn(name = "employees_id")
	private Employee employee;
	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}



	/**
	 * @return the typeRegister
	 */
	public String getTypeRegister() {
		return typeRegister;
	}



	/**
	 * @param typeRegister the typeRegister to set
	 */
	public void setTypeRegister(String typeRegister) {
		this.typeRegister = typeRegister;
	}



	/**
	 * @return the registersDate
	 */
	public LocalDateTime getRegistersDate() {
		return registersDate;
	}

	@PrePersist
	public void prePersist() {
		LocalDateTime.now();
	}


	/**
	 * @param registersDate the registersDate to set
	 */
	public void setRegistersDate(LocalDateTime registersDate) {
		this.registersDate = registersDate;
	}
	



	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	private static final long serialVersionUID = 1L;
}
