package co.com.cidenet.app.dto;

import java.time.LocalDateTime;

public class EmployeeDto {

	private String id;
	
	private String lastname;
	
	private String number;
	
	private String name;
	
	private String email;
	
	private String typeRegister;
	
	private LocalDateTime registersDate;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the typeRegister
	 */
	public String getTypeRegister() {
		return typeRegister;
	}

	/**
	 * @param typeRegister the typeRegister to set
	 */
	public void setTypeRegister(String typeRegister) {
		this.typeRegister = typeRegister;
	}

	/**
	 * @return the registersDate
	 */
	public LocalDateTime getRegistersDate() {
		return registersDate;
	}

	
	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @param registersDate the registersDate to set
	 */
	public void setRegistersDate(LocalDateTime registersDate) {
		this.registersDate = registersDate;
	}
	
}
