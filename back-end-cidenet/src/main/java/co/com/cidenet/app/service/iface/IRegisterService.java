package co.com.cidenet.app.service.iface;

import org.springframework.data.domain.Page;


import co.com.cidenet.app.model.Register;

public interface IRegisterService {
	public Register save(Register employee);
	
	public Page<Register> show();
}
