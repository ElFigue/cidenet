package co.com.cidenet.app.controller;




import javax.security.auth.login.CredentialNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.com.cidenet.app.model.Employee;
import co.com.cidenet.app.model.Register;
import co.com.cidenet.app.service.iface.IEmployeeService;
import co.com.cidenet.app.service.iface.IRegisterService;


/**
 * 
 * @author Cristian
 * Controlador Rest API para Empleados
 *
 */
@RestController
@RequestMapping("/employee")
@CrossOrigin("*")
public class EmployeeController {
	private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private IEmployeeService employeeService;
	
	@PostMapping
	public Employee save(@RequestBody Employee employee) {
		try {
			return employeeService.save(employee);
		} catch (Exception e) {
			log.error("Error al guardar: " + e.getMessage());
			return new Employee();
		}
	}
	
	
	
	//mostramos todos los empleados
	@GetMapping
	public Page<Employee> getAll(){
		return employeeService.show();
	}
	
	// operacion para borrar empleados por id
	@DeleteMapping
	public void deleteById(String id) throws CredentialNotFoundException{
		employeeService.delete(id);
	}
	
	
	
	
}
