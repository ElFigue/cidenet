package co.com.cidenet.app.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.cidenet.app.model.Register;

public interface IRegisterRepository extends CrudRepository<Register, Long>{

}
