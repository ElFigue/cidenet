package co.com.cidenet.app.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "employees")
public class Employee implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	@Pattern(regexp = "[A-Za-z0-9]+$")
	private String number;
	
	@NotEmpty(message = "Apellido obligatorio")
	@Length(max = 20, message = "Maximo 20 caracteres permitidos")
	@Pattern(regexp = "[A-Z^Ññ]+")
	private String lastname;
	
	@NotEmpty(message = "Segundo Apellido obligatorio")
	@Length(max = 20, message = "Maximo 20 caracteres permitidos")
	@Pattern(regexp = "[A-Z^Ññ]+")
	private String surname;
	
	@NotEmpty(message = "Nombre obligatorio")
	@Length(max = 20, message = "Maximo 20 caracteres permitidos")
	@Pattern(regexp = "[A-Z^Ññ]+")
	private String name;
	
	@Length(max = 50, message = "Maximo 50 caracteres permitidos")
	@Pattern(regexp = "[A-Z^Ññ ]+")
	@Column(name = "middle_name")
	private String middleName;
	
	@Email
	private String email;
	
	@NotNull
	private Boolean status;
	
	@ManyToOne
	@JoinColumn(name = "countries_id")
	private Country country;
	
	@ManyToOne
	@JoinColumn(name = "type_id_id")
	private TypeId typeId;
	
	@ManyToOne
	@JoinColumn(name = "areas_id")
	private Area area;
	
	@OneToMany(mappedBy = "employee")
	private List<Register> registers;
	
	@Column(name = "created_at")
	private LocalDateTime createdAt;
	
	
	
	@PrePersist
	public void prePersist() {
		createdAt=LocalDateTime.now();
		
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}



	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}



	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}



	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}



	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}



	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}



	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}



	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}



	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}



	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}



	/**
	 * @return the country
	 */
	public Country getCountry() {
		return country;
	}



	/**
	 * @param country the country to set
	 */
	public void setCountry(Country country) {
		this.country = country;
	}



	/**
	 * @return the typeId
	 */
	public TypeId getTypeId() {
		return typeId;
	}



	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId(TypeId typeId) {
		this.typeId = typeId;
	}



	/**
	 * @return the area
	 */
	public Area getArea() {
		return area;
	}


	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(String number) {
		this.number = number;
	}

	

	/**
	 * @param area the area to set
	 */
	public void setArea(Area area) {
		this.area = area;
	}
	

	
	private static final long serialVersionUID = 1L;

	
}
