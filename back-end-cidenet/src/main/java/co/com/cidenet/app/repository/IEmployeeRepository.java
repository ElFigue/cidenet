package co.com.cidenet.app.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import co.com.cidenet.app.model.Country;
import co.com.cidenet.app.model.Employee;
import co.com.cidenet.app.model.TypeId;

@Repository
public interface IEmployeeRepository extends CrudRepository<Employee, String>{

	public Page<Employee> findAll(Pageable pageable);
	
	public Employee findByNameAndMiddleNameAndLastnameAndSurnameAndTypeIdAndIdAndCountryAndEmailAndStatus(String name,
			String middleName, String lastname, String surname, TypeId typeId, String id, Country country, 
			String email,Boolean status);
	
	@Query("SELECT e FROM Employee e WHERE e.email = ?1")
	public Employee findByEmail(String email);

	
	
	public Employee findById(Long id);
	
}
