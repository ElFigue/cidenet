package co.com.cidenet.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.com.cidenet.app.model.Country;

public interface ICountryRepository extends CrudRepository<Country, Long>{
	public Optional<Country> findById(Long id);
}
