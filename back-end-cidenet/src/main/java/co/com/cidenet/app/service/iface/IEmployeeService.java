package co.com.cidenet.app.service.iface;
import javax.security.auth.login.CredentialNotFoundException;


import org.springframework.data.domain.Page;

import co.com.cidenet.app.model.Employee;

// creamos la intefaz de los empleados
public interface IEmployeeService {
	
	public Employee save(Employee employee) throws Exception;
	
	public Page<Employee> show();
		
	public void delete(String id) throws  CredentialNotFoundException;
	
	
}
