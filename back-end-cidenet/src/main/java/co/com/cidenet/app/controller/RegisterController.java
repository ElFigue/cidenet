package co.com.cidenet.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import co.com.cidenet.app.model.Register;

import co.com.cidenet.app.service.iface.IRegisterService;

@RestController
@RequestMapping("/register")
@CrossOrigin("*")
public class RegisterController {
	@Autowired
	private IRegisterService registerService;
	
	@PostMapping
	public Register save(@RequestBody Register register) {
		return registerService.save(register);
	}
	@GetMapping
	public Page<Register> getAll(){
		return registerService.show();
	}
	
}
