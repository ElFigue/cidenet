package co.com.cidenet.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndCidenetApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndCidenetApplication.class, args);
	}

}
