package co.com.cidenet.app.service.impl;

import java.util.Optional;

import javax.security.auth.login.CredentialNotFoundException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.cidenet.app.model.Country;
import co.com.cidenet.app.model.Employee;
import co.com.cidenet.app.repository.ICountryRepository;
import co.com.cidenet.app.repository.IEmployeeRepository;
import co.com.cidenet.app.service.iface.IEmployeeService;


@Service
public class EmployeeService implements IEmployeeService{
	
	
	@Autowired
	private IEmployeeRepository employeeRepository; 
	
	@Autowired
	private ICountryRepository countryRepository;
	
	
	//creamos el email con los datos de los empleados
	@Override
	@Transactional
	public Employee save(Employee employee) throws Exception{
		String email = "";
		String [] emailName = new String[3];
		emailName[0]=employee.getName();
		emailName[1] = employee.getLastname().replace(" ","");
		Optional<Country> country = countryRepository.findById(employee.getCountry().getId());
		String countryStr;
		
		//asignamos el dominio al correo dependiendo del pais.
		if(country.isPresent()) {
			if(country.get().getId()==1) {
				countryStr = "cidenet.com.co";
			}else {
				countryStr = "cidenet.com.us";
			}
		}else {
			return new Employee();
		}
		Employee employeeNew;
		
		//manejamos los correos repetidos.
		employeeNew = employeeRepository
				.findByEmail(emailName[0].concat(".").concat(emailName[1]).concat("@").concat(countryStr));
		if(employeeNew != null) {
			int i = 1;
			do {
				emailName[2] = i+"";
				employeeNew = employeeRepository.findByEmail(emailName[0].concat(".").concat(emailName[1]).concat(".").concat(emailName[2]).concat("@").concat(countryStr));
				if(employeeNew != null) {
					i++;
				}else {
					email = emailName[0].concat(".").concat(emailName[1]).concat(".").concat(emailName[2]).concat("@").concat(countryStr);
					break;
				}
			}while(employeeNew != null);
		}else {
			email = emailName[0].concat(".").concat(emailName[1]).concat("@").concat(countryStr);
		}
		employee.setEmail(email);
		return employeeRepository.save(employee);
	}

	
	/*
	 * agregamos la paginación.
	 */
	@Override
	public Page<Employee> show() {
		Pageable paging = PageRequest.of(0, 10);
		return employeeRepository.findAll(paging);
	}

	@Override
	@Transactional
	public void delete(String id) throws CredentialNotFoundException {
		employeeRepository.deleteById(id);
	}

	
	
	public  Employee edit(Employee entity) {
		if (entity.getId() == null) {
			entity = employeeRepository.save(entity);
			return entity;
		
	}else {
		Optional<Employee> employee = employeeRepository.findById(entity.getId());
		if (employee.isPresent()) {
			Employee newEntity = employee.get();
			newEntity.setName(entity.getName());
			newEntity.setLastname(entity.getLastname());
			newEntity.setMiddleName(entity.getMiddleName());
			newEntity.setSurname(entity.getSurname());
			newEntity.setEmail(entity.getEmail());
			newEntity.setArea(entity.getArea());
			newEntity.setCountry(entity.getCountry());
			newEntity.setId(entity.getId());
			newEntity.setNumber(entity.getNumber());
			newEntity.setTypeId(entity.getTypeId());
			

			newEntity = employeeRepository.save(newEntity);
			return newEntity;
		} else {
			entity = employeeRepository.save(entity);
			return entity;
}
	}
	}


	
}

	
	
