DROP DATABASE cidenet;

CREATE DATABASE cidenet;

USE cidenet;

CREATE TABLE areas(
    id int NOT NULL AUTO_INCREMENT,
    name VARCHAR(120) NOT NULL,
    description TEXT NULL,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE type_id(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(120) NOT NULL,
    description TEXT NULL,
    created_at DATETIME NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id)
);

CREATE TABLE countries(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE employees(
    id INT NOT NULL AUTO_INCREMENT,
    number VARCHAR(20) UNIQUE,
    lastname VARCHAR(20) NOT NULL,
    surname VARCHAR(20) NOT NULL,
    name VARCHAR(20) NOT NULL,
    middle_name VARCHAR(50) NULL,
    countries_id INT NOT NULL,
    type_id_id INT NOT NULL,
    email VARCHAR(300) NOT NULL UNIQUE,
    areas_id INT NOT NULL,
    status TINYINT NULL DEFAULT 1,
    PRIMARY KEY(id),
    FOREIGN KEY (countries_id) REFERENCES countries(id),
    FOREIGN KEY (type_id_id) REFERENCES type_id(id),
    FOREIGN KEY (areas_id) REFERENCES areas(id)
);

CREATE TABLE registers(
    id INT NOT NULL AUTO_INCREMENT,
    type_register VARCHAR(20) NOT NULL, -- entrada o salida
    registers_date DATETIME NOT NULL,
    employees_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY (employees_id) REFERENCES employees(id)
);

/*POPULATION*/
-- areas
INSERT INTO areas(name,description)
VALUES ('administracion','');

INSERT INTO areas(name,description)
VALUES ('financiera','');

INSERT INTO areas(name,description)
VALUES ('compras','');

INSERT INTO areas(name,description)
VALUES ('infraestructura','');

INSERT INTO areas(name,description)
VALUES ('operacion','');

INSERT INTO areas(name,description)
VALUES ('talento humano','');

INSERT INTO areas(name,description)
VALUES ('servicios varios','');

INSERT INTO areas(name,description)
VALUES ('otra','otra área');

-- type_id
INSERT INTO type_id(name,description)
VALUES ('cedula de ciudadania','');

INSERT INTO type_id(name,description)
VALUES ('cedula de extranjeria','');

INSERT INTO type_id(name,description)
VALUES ('pasaporte','');

INSERT INTO type_id(name,description)
VALUES ('permiso especial','');

-- countries
INSERT INTO countries(name)
VALUES ('colombia');-- 1

INSERT INTO countries(name)
VALUES ('estados unidos');-- 2