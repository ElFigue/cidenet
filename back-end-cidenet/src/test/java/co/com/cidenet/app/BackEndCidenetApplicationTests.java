package co.com.cidenet.app;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.repository.CrudRepository;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
class BackEndCidenetApplicationTests {
	
	@Test
	void contextLoads() {
		
	}
	
	@MockBean
	private CrudRepository<?, ?> userRepository;
	

}
